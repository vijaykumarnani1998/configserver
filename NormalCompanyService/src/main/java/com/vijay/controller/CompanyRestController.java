package com.vijay.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vijay.exception.CompanyNotFoundException;
import com.vijay.model.Company;
import com.vijay.service.CompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyRestController {

	@Autowired
	private CompanyService service;

	//1. create company
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(@RequestBody Company company) 
	{
		log.info("ENTERED INTO SAVE METHOD");
		ResponseEntity<String> response = null;
		try {
			Long id = service.createCompany(company);
			String message="COMPANY '"+id+"' CREATED ";
			response = new ResponseEntity<String>(message,HttpStatus.CREATED); 
			log.info("COMPANY IS CREATED {}.",id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		return response;
	}

	//2. fetch all
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllCompanies() 
	{
		ResponseEntity<List<Company>> response = null;
		log.info("ENTERED INTO FETCH METHOD");
		try {
			List<Company> list = service.getAllCompanies();
			response = ResponseEntity.ok(list);
			log.info("FETCH METHOD IS SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE FETCH ALL METHOD");
		return response;
	}

	//3. fetch one
	@GetMapping("/fetch/{id}")
	public ResponseEntity<Company> getOneCompany(@PathVariable Long id)
		
	{
		ResponseEntity<Company> response = null;
		log.info("ENTERED INTO FETCH ONE METHOD");
		try {
			Company company = service.getOneCompany(id);
			
			response = ResponseEntity.ok(company);
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (CompanyNotFoundException e) {
			//e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE FETCH ONE METHOD");
		return response;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteCompany(@PathVariable Long id)
		
	{
		ResponseEntity<String> response = null;
		log.info("ENTERED INTO DELETE METHOD");
		try {
		 service.deleteCompany(id);
			response = ResponseEntity.ok("COMPANY '"+id+"' Deleted");
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (CompanyNotFoundException e) {
			//e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("LEAVING DELETE METHOD");
		return response;
	}
	@PutMapping("/update")
	public ResponseEntity<String> updateCompany(@RequestBody Company company) 
	{
		log.info("ENTERED INTO UPDATE METHOD");
		ResponseEntity<String> response = null;
		try {
		    service.updateCompany(company);
			response = ResponseEntity.ok("UPDATED WITH ID : " + company.getId());
			log.info("COMPANY IS CREATED {}.",company.getId());
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("LEAVING UPDATE METHOD");
		return response;
	}

	
}


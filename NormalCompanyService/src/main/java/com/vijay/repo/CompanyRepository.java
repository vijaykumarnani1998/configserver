package com.vijay.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vijay.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}

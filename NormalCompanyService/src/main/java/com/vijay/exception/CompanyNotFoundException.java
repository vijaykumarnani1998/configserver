package com.vijay.exception;

public class CompanyNotFoundException extends RuntimeException {

	
private static final long serialVersionUID = 5562717983502518561L;


public CompanyNotFoundException() 
{
	super();
}

public CompanyNotFoundException(String message)
{
	super(message);
}
}

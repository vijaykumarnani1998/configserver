package com.vijay.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vijay.exception.CompanyNotFoundException;

@RestControllerAdvice
public class CompanyCustomExceptionHandler {
	
	@ExceptionHandler (CompanyNotFoundException.class)
    public ResponseEntity<String> handleException(CompanyNotFoundException e)
    {
		return new   ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

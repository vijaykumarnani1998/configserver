package com.vijay.service;

import java.util.List;

import com.vijay.model.Company;

public interface CompanyService {

	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();
    void deleteCompany(Long id);
}

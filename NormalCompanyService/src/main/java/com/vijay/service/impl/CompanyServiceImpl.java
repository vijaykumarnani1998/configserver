package com.vijay.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vijay.exception.CompanyNotFoundException;
import com.vijay.model.Company;
import com.vijay.repo.CompanyRepository;
import com.vijay.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository repo;
	
	@Override
	public Long createCompany(Company cob) {
		return repo.save(cob).getId();
	}

	@Override
	public void updateCompany(Company cob) {
		if(cob.getRegId()!=null && repo.existsById(cob.getId()))
			repo.save(cob);
	}

	@Override
	public Company getOneCompany(Long id) {
		/*
		 * Optional<Company> opt = repo.findById(id); Company company=opt.get(); return
		 * company;
		 */
		
		Optional<Company> opt = repo.findById(id);
		if(opt.isEmpty()) {
			throw new CompanyNotFoundException("Given '"+id+"' Not exist");
		} else {
			return opt.get();
		}

	}

	@Override
	public List<Company> getAllCompanies() {
		return repo.findAll();
	}

	@Override
	public void deleteCompany(Long id) {
		Company company= getOneCompany(id);
      repo.delete(company);
	}

}
